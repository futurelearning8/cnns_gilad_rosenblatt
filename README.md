# Neural Networks

This is the convolution neural networks exercise for FL8 week 5 day 2 by Gilad Rosenblatt.

### Run

Run `main.py` from `code` as working directory.

### Results

Best version of the VGG-16 adaptation for CIFAR10 dataset used Adam optimizer with a learning rate drop policy, batch normalization layers, dropout, and a global average pooling layer (instead of fully connected layers).

**The model achieved a validation accuracy score of 0.885.**

Model accuracy on validation set: 88.46% (with loss = 0.5883898735046387)

Model accuracy on train set: 99.96% (with loss = 0.0027).

The model architecture is as follows:

![model](/images/arc.png?raw=true).

## License

[WTFPL](http://www.wtfpl.net/)
