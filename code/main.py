import os
import tensorflow as tf
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from time import time
from tensorflow.python.keras.callbacks import TensorBoard
from sklearn.metrics import confusion_matrix


class VGG:
    """VGG model."""

    def __init__(self):
        """Initialize the model (build neural network architecture)."""

        # Build model.
        self.model = tf.keras.Sequential()
        self._build(
            input_shape=(32, 32, 3),
            block_filters=[64, 128, 256, 512],
            block_repetitions=[2, 2, 3, 3],
            dropout_rate=0.3
        )

        # Directory to save logs for tensorboard.
        self.tensorboard = TensorBoard(f"../log/{time()}")

        # Directory to save checkpoints during training.
        self.checkpoint_path = "../training/cp-{epoch:02d}.ckpt"
        self.checkpoint_dir = os.path.dirname(self.checkpoint_path)

        # Directory to save model after training.
        self.model_path = "../models/my_model"
        self.model_dir = os.path.dirname(self.model_path)

    def _build(self, input_shape, block_filters, block_repetitions, dropout_rate=0.3):
        """
        Build a modified VGG-16-based model architecture.

        :param tuple input_shape: shape of the inputs.
        :param list block_filters: number of filters to use for each convolution layer in a convolution + pooling block.
        :param list block_repetitions: number of convolutional layers to use in each convolution + pooling block.
        :param float dropout_rate: the dropout rate to use for the dropout layer before the softmax layer.
        """

        # Set input size.
        self.model.add(tf.keras.layers.InputLayer(input_shape=input_shape, name="input"))

        # Add convolutional and pooling blocks.
        for block_index, (filters, repetitions), in enumerate(zip(block_filters, block_repetitions)):

            # Add convolutional + batch normalization + activation layers.
            for conv_layer_index in range(repetitions):
                self.model.add(tf.keras.layers.Conv2D(
                    filters=filters,
                    kernel_size=(3, 3),
                    padding="same",
                    name=f"block_{block_index + 1}_conv_{conv_layer_index + 1}"
                ))
                self.model.add(tf.keras.layers.BatchNormalization(
                    name=f"block_{block_index + 1}_batch_norm_{conv_layer_index + 1}"
                ))
                self.model.add(tf.keras.layers.Activation(
                    activation=tf.nn.relu,
                    name=f"block_{block_index + 1}_relu_{conv_layer_index + 1}"
                ))

            # Add pooling layer.
            self.model.add(tf.keras.layers.MaxPooling2D(
                pool_size=(2, 2),
                strides=(2, 2),
                name=f"block_{block_index + 1}_pooling"
            ))

        # Transition to a flattened layer: add global average pooling and flatten.
        self.model.add(tf.keras.layers.GlobalAveragePooling2D(name="global_pooling"))
        self.model.add(tf.keras.layers.Flatten(name="flatten"))

        # Add dropout at last layer.
        self.model.add(tf.keras.layers.Dropout(dropout_rate, name="dropout"))

        # Add softmax for 10 classes.
        self.model.add(tf.keras.layers.Dense(10, activation=tf.nn.softmax, name="softmax"))

    def fit(self, x_train, y_train, x_test, y_test, batch_size, epochs, learning_rate=0.001):
        """
        Define an optimizer, compile the model, fit it on the train set and evaluate it on the validation set. Use
        callbacks to save weights at checkpoints and manage the learning rate. Save the model once training is complete.

        :param np.ndarray x_train: train set images.
        :param np.ndarray y_train: train set labels (one-hot encoded).
        :param np.ndarray x_test: test set images.
        :param np.ndarray y_test: test set labels (one-hot encoded).
        :param int batch_size: size of a single batch for training.
        :param int epochs: number of epochs to train the model for.
        :param float learning_rate: learning rate to use for the Adam optimizer.
        """

        # Set learning update policy rules.
        factor = 0.2
        patience = 2
        min_learning_rate = learning_rate / 100

        # Define optimizer.
        optimizer = tf.keras.optimizers.Adam(
            learning_rate=learning_rate
        )

        # Compile the model and optimizer.
        self.model.compile(
            loss="categorical_crossentropy",
            optimizer=optimizer,
            metrics=["accuracy"]
        )

        # Create a callback to save model weights every epoch (include epoch in filename).
        save_checkpoint = tf.keras.callbacks.ModelCheckpoint(
            filepath=self.checkpoint_path,
            verbose=1,
            save_weights_only=True)

        # Create a callback to reduce learning rate on plateau.
        reduce_learning_rate = tf.keras.callbacks.ReduceLROnPlateau(
            monitor="val_loss",
            factor=factor,
            patience=patience,
            min_lr=min_learning_rate
        )

        # Save initial weights using the checkpoint_path format.
        self.model.save_weights(self.checkpoint_path.format(epoch=0))

        # Train the model w/callbacks.
        self.model.fit(
            x=x_train,
            y=y_train,
            batch_size=batch_size,
            validation_data=(x_test, y_test),
            epochs=epochs,
            callbacks=[save_checkpoint, reduce_learning_rate, self.tensorboard],
            shuffle=True,
            verbose=1
        )

        # Evaluate trained model on validation set.
        loss, accuracy = self.model.evaluate(x=x_test, y=y_test, verbose=2)
        print(f"Model accuracy on validation set: {100 * accuracy:5.2f}%")

        # Save the model.
        self.model.save(
            f"{self.model_path}_"
            f"bs{batch_size}_"
            f"ep{epochs}_"
            f"acc{accuracy:.3f}"
        )


def main():
    # Load dataset.
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.cifar10.load_data()

    # Save test labels for confusion matrix calculation.
    y_test_labels = y_test.copy()

    # Convert labels to 1-hot encodings.
    y_train = tf.keras.utils.to_categorical(y_train, 10)
    y_test = tf.keras.utils.to_categorical(y_test, 10)

    # Initialize model and show summary.
    vgg = VGG()
    vgg.model.summary()

    # Compile the model with an optimizer and fit to train set using accuracy metric (print evaluation at the end).
    vgg.fit(
        x_train=x_train,
        y_train=y_train,
        x_test=x_test,
        y_test=y_test,
        batch_size=64,
        epochs=15,
        learning_rate=0.001
    )

    # Define label interpretation from <https://www.tensorflow.org/api_docs/python/tf/keras/datasets/cifar10/load_data>.
    label_interpretations = {
        0: "airplane",
        1: "automobile",
        2: "bird",
        3: "cat",
        4: "deer",
        5: "dog",
        6: "frog",
        7: "horse",
        8: "ship",
        9: "truck"
    }
    categories = list(label_interpretations.values())

    # Plot confusion matrix.
    cf_matrix = confusion_matrix(
        y_true=y_test_labels,
        y_pred=np.argmax(vgg.model.predict(x_test, batch_size=32, verbose=1), axis=-1)  # Use label indices (not 1-hot).
    )
    sns.heatmap(
        cf_matrix,
        annot=True,
        cmap='Blues',
        xticklabels=categories,
        yticklabels=categories
    )
    plt.show()


def load_best_model_and_score():
    # Load dataset.
    _, (x_test, y_test) = tf.keras.datasets.cifar10.load_data()

    # Convert labels to 1-hot encodings.
    y_test = tf.keras.utils.to_categorical(y_test, 10)

    # Load saved trained model (epoch 9 checkpoint has best validation score).
    model = tf.keras.models.load_model("../models/my_model_bs64_ep15_bnTrue_doTrue_testacc0.8845999836921692")
    model.summary()

    # Evaluate trained model on validation set.
    loss, accuracy = model.evaluate(x=x_test, y=y_test, verbose=2)
    print(f"Model accuracy on validation set: {100 * accuracy:5.2f}% (with loss = {loss})")


if __name__ == "__main__":
    # main()
    load_best_model_and_score()
